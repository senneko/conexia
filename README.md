
# Aplicación de factura
## LA MEJOR COCINA
Archivos incluidos:

 - script.sql - script de creación de base de datos
 - factura-app.jar - aplicación web
 - backend/ - fuente de servicios back-end
 - frontend/ - fuente de aplicación front-end
 - 

El motor de base de datos es MySQL v5.7.

La aplicación esta configurada para ser ejecutada con un usuario de base de datos:
usuario: conexia
clave: \*Manager2019*

En caso de que se requiera modificar, es necesario ingresar al archivo **factura-app.jar** con un gestor de archivos y modificar el siguiente archivo:

**/BOOT-INF/classes/application.properties**

Las llaves que se deben modificar son:

*db.username*
*db.password*

Para ejecutar la aplicación es necesario tener java8 y ejecutar el siguiente comando:
*$ java -jar factura-app.jar*

Una vez la aplicación este arriba, ingresar a un navegador con la siguiente url:

http://localhost:8080/factura

Se tiene un menu en la parte izquierda:

 - Factura: Registro de facturas
 - Reporte 1: Reporte de camareros
 - Reporte 2: Reporte de clientes
