import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Factura, DetalleFactura } from "./../interfaces/dominio.interface";

@Injectable({
  providedIn: 'root'
})
export class FacturaService {

  API = "http://localhost:8080";

  constructor(private http: HttpClient) { }

  getCamareros() {
    let promise = new Promise((resolve, reject) => {
      this.http.post(this.API + '/factura/getcamareros', null)
        .subscribe((data) => {
          resolve(data);
        });
    });
    return promise;
  }

  getMesas() {
    let promise = new Promise((resolve, reject) => {
      this.http.post(this.API + '/factura/getmesas', null)
        .subscribe((data) => {
          resolve(data);
        });
    });
    return promise;
  }

  getCocineros() {
    let promise = new Promise((resolve, reject) => {
      this.http.post(this.API + '/factura/getcocineros', null)
        .subscribe((data) => {
          resolve(data);
        });
    });
    return promise;
  }

  getReporte1() {
    let promise = new Promise((resolve, reject) => {
      this.http.post(this.API + '/factura/getreporte1', null)
        .subscribe((data) => {
          resolve(data);
        });
    });
    return promise;
  }

  getReporte2() {
    let promise = new Promise((resolve, reject) => {
      this.http.post(this.API + '/factura/getreporte2', null)
        .subscribe((data) => {
          resolve(data);
        });
    });
    return promise;
  }

  setFactura(factura: Factura) {
    let promise = new Promise((resolve, reject) => {
      this.http.post(this.API + '/factura/setfactura', factura)
        .subscribe((data: any) => {
          console.log(data);
          if (data) {
            if (data.success) {
              resolve();
            } else {
              reject(data.message);
            }
          } else {
            reject("data null");
          }
        });
    });
    return promise;
  }
}
