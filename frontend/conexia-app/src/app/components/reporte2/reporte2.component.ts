import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material';
import { FacturaService } from 'src/app/services/factura.service';

@Component({
  selector: 'app-reporte2',
  templateUrl: './reporte2.component.html',
  styleUrls: ['./reporte2.component.css']
})
export class Reporte2Component implements OnInit {

  dataSource = new MatTableDataSource(data);

  displayedColumns: string[] = ['nombre', 'total'];

  constructor(private facturaService: FacturaService) { }

  ngOnInit() {
    this.facturaService.getReporte2().then((reporte: any[]) => {
      data = reporte;
      this.dataSource = new MatTableDataSource(data);
    });
  }

}

let data: any[] = [];