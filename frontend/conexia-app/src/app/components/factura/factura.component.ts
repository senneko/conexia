import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { FacturaService } from 'src/app/services/factura.service';
import { DetalleFactura, Factura } from 'src/app/interfaces/dominio.interface';
import { MatTableDataSource } from '@angular/material';

@Component({
  selector: 'app-factura',
  templateUrl: './factura.component.html',
  styleUrls: ['./factura.component.css']
})

export class FacturaComponent implements OnInit {

  camareros = [];
  mesas = [];
  cocineros = [];

  factura: Factura;
  detalle: DetalleFactura;

  dataSource = new MatTableDataSource(data);

  facturaForm: FormGroup;
  detalleForm: FormGroup;

  displayedColumns: string[] = ['plato', 'valor', 'cocinero'];

  constructor(private formBuilder: FormBuilder, private facturaService: FacturaService) { }

  ngOnInit() {
    this.facturaForm = this.formBuilder.group({
      nombre: ['', [Validators.compose([Validators.required, Validators.maxLength(45)])]],
      apellido1: ['', [Validators.compose([Validators.required, Validators.maxLength(45)])]],
      apellido2: ['', [Validators.compose([Validators.maxLength(45)])]],
      idCamarero: new FormControl('', [Validators.required]),
      idMesa: new FormControl('', [Validators.required]),
    });
    this.detalleForm = this.formBuilder.group({
      plato: ['', [Validators.compose([Validators.required, Validators.maxLength(45)])]],
      valor: ['', [Validators.compose([Validators.required, Validators.maxLength(10)])]],
      idCocinero: new FormControl('', [Validators.required]),
    });
    this.facturaService.getCamareros().then((data: any[]) => {
      this.camareros = data;
    });
    this.facturaService.getMesas().then((data: any[]) => {
      this.mesas = data;
    });
    this.facturaService.getCocineros().then((data: any[]) => {
      this.cocineros = data;
    });
  }

  save() {
    if (this.facturaForm.valid && data.length > 0) {
      this.factura = {
        nombreCliente: new String(this.facturaForm.value.nombre).trim(),
        apellido1Cliente: new String(this.facturaForm.value.apellido1).trim(),
        apellido2Cliente: new String(this.facturaForm.value.apellido2).trim(),
        idCamarero: new Number(this.facturaForm.value.idCamarero),
        idMesa: new Number(this.facturaForm.value.idCamarero),
        detalleFactura: data,
      };
      this.facturaService.setFactura(this.factura).then(() => {
        //limpiar todo para una nueva factura
        data = [];
        this.dataSource = new MatTableDataSource(data);
        this.detalleForm.reset();
        this.facturaForm.reset();
      }).catch((error) => {
        console.error(error);
      });
    }
  }

  add() {
    if (this.detalleForm.valid) {
      this.detalle = {
        plato: new String(this.detalleForm.value.plato).trim(),
        valor: new Number(this.detalleForm.value.valor),
        idCocinero: new Number(this.detalleForm.value.idCocinero),
      };
      data.push(this.detalle);
      this.dataSource = new MatTableDataSource(data);
      this.detalleForm.reset();
    }
  }

}

let data: DetalleFactura[] = [];