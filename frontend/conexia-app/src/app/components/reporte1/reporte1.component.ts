import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material';
import { FacturaService } from 'src/app/services/factura.service';

@Component({
  selector: 'app-reporte1',
  templateUrl: './reporte1.component.html',
  styleUrls: ['./reporte1.component.css']
})
export class Reporte1Component implements OnInit {

  dataSource = new MatTableDataSource(data);

  displayedColumns: string[] = ['nombre', 'apellido', 'mes', 'total'];

  constructor(private facturaService: FacturaService) { }

  ngOnInit() {
    this.facturaService.getReporte1().then((reporte: any[]) => {
      data = reporte;
      this.dataSource = new MatTableDataSource(data);
    });
  }

}

let data: any[] = [];