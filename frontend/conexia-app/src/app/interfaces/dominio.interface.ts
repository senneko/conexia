export interface DetalleFactura {
    plato: String;
    valor: Number;
    idCocinero: Number;
}

export interface Factura {
    nombreCliente: String;
    apellido1Cliente: String;
    apellido2Cliente?: String;
    idCamarero: Number;
    idMesa: Number;
    detalleFactura: DetalleFactura[];
}