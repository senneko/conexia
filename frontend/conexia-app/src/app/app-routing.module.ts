import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { WelcomeComponent } from "./components/welcome/welcome.component";
import { FacturaComponent } from "./components/factura/factura.component";
import { Reporte1Component } from "./components/reporte1/reporte1.component";
import { Reporte2Component } from "./components/reporte2/reporte2.component";

const APP_ROUTES: Routes = [
  { path: '', component: WelcomeComponent },
  { path: 'factura', component: FacturaComponent },
  { path: 'reporte1', component: Reporte1Component },
  { path: 'reporte2', component: Reporte2Component },
  { path: '**', pathMatch: 'full', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forRoot(APP_ROUTES, {useHash:false})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
