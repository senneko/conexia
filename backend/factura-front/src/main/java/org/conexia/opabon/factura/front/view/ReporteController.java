package org.conexia.opabon.factura.front.view;

import java.util.List;
import org.conexia.opabon.factura.ejb.CamareroService;
import org.conexia.opabon.factura.ejb.ClienteService;
import org.conexia.opabon.factura.ejb.domain.BasicResponse;
import org.conexia.opabon.factura.rpt.domain.CamareroReporte;
import org.conexia.opabon.factura.rpt.domain.ClienteReporte;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

//@CrossOrigin
@RestController()
public class ReporteController {
    
    @Autowired
    private CamareroService camareroService;
    
    @Autowired
    private ClienteService clienteService;
    
    @RequestMapping(value = {"/getreporte1" }, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<List<CamareroReporte>> getReporte1() {
        List<CamareroReporte> reporte = camareroService.getCamareroReporte();
        return new ResponseEntity<>(reporte, HttpStatus.OK);
    }
    
    @RequestMapping(value = {"/getreporte2" }, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<List<ClienteReporte>> getReporte2() {
        List<ClienteReporte> reporte = clienteService.getClienteReporte();
        return new ResponseEntity<>(reporte, HttpStatus.OK);
    }
}
