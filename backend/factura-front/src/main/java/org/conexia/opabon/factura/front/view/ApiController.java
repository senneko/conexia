package org.conexia.opabon.factura.front.view;

import java.util.List;
import org.conexia.opabon.factura.ejb.CamareroService;
import org.conexia.opabon.factura.ejb.CocineroService;
import org.conexia.opabon.factura.ejb.MesaService;
import org.conexia.opabon.factura.ejb.domain.DmnCamarero;
import org.conexia.opabon.factura.ejb.domain.DmnCocinero;
import org.conexia.opabon.factura.ejb.domain.DmnMesa;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

//@CrossOrigin
@RestController()
public class ApiController {

    @Autowired
    private CamareroService camareroService;
    
    @Autowired
    private MesaService mesaService;
    
    @Autowired
    private CocineroService cocineroService;

    @RequestMapping(value = {"/getcamareros"}, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<List<DmnCamarero>> getCamareros() {
        List<DmnCamarero> dmnCamarero = camareroService.findAll();
        return new ResponseEntity<>(dmnCamarero, HttpStatus.OK);
    }

    @RequestMapping(value = {"/getmesas"}, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<List<DmnMesa>> getMesas() {
        List<DmnMesa> dmnMesa = mesaService.findAll();
        return new ResponseEntity<>(dmnMesa, HttpStatus.OK);
    }

    @RequestMapping(value = {"/getcocineros"}, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<List<DmnCocinero>> getCocineros() {
        List<DmnCocinero> dmnCocinero = cocineroService.findAll();
        return new ResponseEntity<>(dmnCocinero, HttpStatus.OK);
    }
}
