package org.conexia.opabon.factura.front.view;

import org.conexia.opabon.factura.ejb.FacturaService;
import org.conexia.opabon.factura.ejb.domain.BasicResponse;
import org.conexia.opabon.factura.ejb.domain.DmnFactura;
import org.conexia.opabon.factura.ejb.exceptios.SystemException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

//@CrossOrigin
@RestController()
public class FacturaController {
    
    @Autowired
    private FacturaService facturaService;
    
    @RequestMapping(value = {"/setfactura" }, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<BasicResponse> setFactura(@RequestBody DmnFactura factura) {
        try {
            facturaService.setFactura(factura);
            return new ResponseEntity<>(new BasicResponse(true), HttpStatus.OK);
        } catch (SystemException e) {
            return new ResponseEntity<>(new BasicResponse(false, e.getMessage()), HttpStatus.OK);
        }
    }
}
