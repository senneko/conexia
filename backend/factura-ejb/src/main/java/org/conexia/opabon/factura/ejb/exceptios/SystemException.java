/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.conexia.opabon.factura.ejb.exceptios;

/**
 *
 * @author senneko
 */
public class SystemException extends Exception {

    public SystemException(String message, Throwable cause) {
        super(message, cause);
    }
    
}
