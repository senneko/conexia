/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.conexia.opabon.factura.ejb;

import org.conexia.opabon.factura.rpt.domain.DetalleFactura;

/**
 *
 * @author senneko
 */
public interface DetalleFacturaService {

    public <S extends DetalleFactura> S saveAndFlush(S s);
    
}
