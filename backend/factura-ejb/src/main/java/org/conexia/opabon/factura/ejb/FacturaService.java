/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.conexia.opabon.factura.ejb;

import org.conexia.opabon.factura.ejb.domain.DmnFactura;
import org.conexia.opabon.factura.ejb.exceptios.SystemException;

/**
 *
 * @author senneko
 */
public interface FacturaService {

    public void setFactura(DmnFactura dmnfactura) throws SystemException;
    
}
