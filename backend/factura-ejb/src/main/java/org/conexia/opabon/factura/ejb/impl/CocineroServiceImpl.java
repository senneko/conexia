/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.conexia.opabon.factura.ejb.impl;

import java.util.ArrayList;
import java.util.List;
import org.conexia.opabon.factura.ejb.CocineroService;
import org.conexia.opabon.factura.ejb.domain.DmnCocinero;
import org.conexia.opabon.factura.rpt.CocineroRepository;
import org.conexia.opabon.factura.rpt.domain.Cocinero;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author senneko
 */
@Service
public class CocineroServiceImpl implements CocineroService {

    @Autowired
    private CocineroRepository cocineroRepository;

    @Override
    public List<DmnCocinero> findAll() {
        List<Cocinero> rptCocinero = cocineroRepository.findAll();
        List<DmnCocinero> dmnCocinero = new ArrayList<>();
        for (Cocinero cocinero : rptCocinero) {
            dmnCocinero.add(DmnCocinero.getDmnCocinero(cocinero));
        }
        return dmnCocinero;
    }

}
