/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.conexia.opabon.factura.ejb;

import java.util.List;
import org.conexia.opabon.factura.rpt.domain.Cliente;
import org.conexia.opabon.factura.rpt.domain.ClienteReporte;

/**
 *
 * @author senneko
 */
public interface ClienteService {

    public List<Cliente> findAll();

    public <S extends Cliente> S saveAndFlush(S s);
    
    public List<ClienteReporte> getClienteReporte();
    
}
