/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.conexia.opabon.factura.ejb.impl;

import java.util.List;
import org.conexia.opabon.factura.ejb.ClienteService;
import org.conexia.opabon.factura.rpt.ClienteRepository;
import org.conexia.opabon.factura.rpt.domain.Cliente;
import org.conexia.opabon.factura.rpt.domain.ClienteReporte;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author senneko
 */
@Service
public class ClienteServiceImpl implements ClienteService {
    
    @Autowired
    private ClienteRepository clienteRepository;

    @Override
    public <S extends Cliente> S saveAndFlush(S s) {
        return clienteRepository.saveAndFlush(s);
    }

    @Override
    public List<Cliente> findAll() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<ClienteReporte> getClienteReporte() {
        return clienteRepository.getClienteReporte();
    }
    
}
