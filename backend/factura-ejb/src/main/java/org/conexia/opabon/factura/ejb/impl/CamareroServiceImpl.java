/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.conexia.opabon.factura.ejb.impl;

import java.util.ArrayList;
import java.util.List;
import org.conexia.opabon.factura.ejb.CamareroService;
import org.conexia.opabon.factura.ejb.domain.DmnCamarero;
import org.conexia.opabon.factura.rpt.CamareroRepository;
import org.conexia.opabon.factura.rpt.domain.Camarero;
import org.conexia.opabon.factura.rpt.domain.CamareroReporte;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author senneko
 */
@Service
public class CamareroServiceImpl implements CamareroService {
    
    @Autowired
    private CamareroRepository camareroRepository;
    
    @Override
    public List<DmnCamarero> findAll() {
        List<Camarero> rptCamarero = camareroRepository.findAll();
        List<DmnCamarero> dmnCamarero = new ArrayList<>();
        for (Camarero camarero : rptCamarero) {
            dmnCamarero.add(DmnCamarero.getDmnCamarero(camarero));
        }
        return dmnCamarero;
    }

    @Override
    public List<CamareroReporte> getCamareroReporte() {
        return camareroRepository.getCamareroReporte();
    }
    
}
