/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.conexia.opabon.factura.ejb.domain;

import org.conexia.opabon.factura.rpt.domain.Cocinero;

/**
 *
 * @author senneko
 */
public class DmnCocinero {
    
    private Integer idCocinero;
    private String nombre;
    private String apellido1;
    private String apellido2;

    public Integer getIdCocinero() {
        return idCocinero;
    }

    public void setIdCocinero(Integer idCocinero) {
        this.idCocinero = idCocinero;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido1() {
        return apellido1;
    }

    public void setApellido1(String apellido1) {
        this.apellido1 = apellido1;
    }

    public String getApellido2() {
        return apellido2;
    }

    public void setApellido2(String apellido2) {
        this.apellido2 = apellido2;
    }

    public static DmnCocinero getDmnCocinero(Cocinero rptCocinero) {
        DmnCocinero cocinero = new DmnCocinero();
        cocinero.setIdCocinero(rptCocinero.getIdCocinero());
        cocinero.setNombre(rptCocinero.getNombre());
        cocinero.setApellido1(rptCocinero.getApellido1());
        cocinero.setApellido2(rptCocinero.getApellido2());
        return cocinero;
    }
    
}
