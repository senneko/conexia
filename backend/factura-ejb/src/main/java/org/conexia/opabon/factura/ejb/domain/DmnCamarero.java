/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.conexia.opabon.factura.ejb.domain;

import org.conexia.opabon.factura.rpt.domain.Camarero;

/**
 *
 * @author senneko
 */
public class DmnCamarero {
    
    private Integer idCamarero;
    private String nombre;
    private String apellido1;
    private String apellido2;

    public Integer getIdCamarero() {
        return idCamarero;
    }

    public void setIdCamarero(Integer idCamarero) {
        this.idCamarero = idCamarero;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido1() {
        return apellido1;
    }

    public void setApellido1(String apellido1) {
        this.apellido1 = apellido1;
    }

    public String getApellido2() {
        return apellido2;
    }

    public void setApellido2(String apellido2) {
        this.apellido2 = apellido2;
    }

    public static DmnCamarero getDmnCamarero(Camarero rptCamarero) {
        DmnCamarero camarero = new DmnCamarero();
        camarero.setIdCamarero(rptCamarero.getIdCamarero());
        camarero.setNombre(rptCamarero.getNombre());
        camarero.setApellido1(rptCamarero.getApellido1());
        camarero.setApellido2(rptCamarero.getApellido2());
        return camarero;
    }
    
}
