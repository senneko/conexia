/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.conexia.opabon.factura.ejb.impl;

import java.util.Date;
import org.conexia.opabon.factura.ejb.FacturaService;
import org.conexia.opabon.factura.ejb.domain.DmnDetalleFactura;
import org.conexia.opabon.factura.ejb.domain.DmnFactura;
import org.conexia.opabon.factura.ejb.exceptios.SystemException;
import org.conexia.opabon.factura.rpt.ClienteRepository;
import org.conexia.opabon.factura.rpt.DetalleFacturaRepository;
import org.conexia.opabon.factura.rpt.FacturaRepository;
import org.conexia.opabon.factura.rpt.domain.Camarero;
import org.conexia.opabon.factura.rpt.domain.Cliente;
import org.conexia.opabon.factura.rpt.domain.Cocinero;
import org.conexia.opabon.factura.rpt.domain.DetalleFactura;
import org.conexia.opabon.factura.rpt.domain.Factura;
import org.conexia.opabon.factura.rpt.domain.Mesa;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author senneko
 */
@Service
public class FacturaServiceImpl implements FacturaService {

    @Autowired
    private FacturaRepository facturaRepository;
    @Autowired
    private ClienteRepository clienteRepository;
    @Autowired
    private DetalleFacturaRepository detalleFacturaRepository;

    @Override
    public void setFactura(DmnFactura dmnfactura) throws SystemException {
        try {
            Factura factura = new Factura();
            factura.setFecha(new Date());
            factura.setIdCamarero(new Camarero(dmnfactura.getIdCamarero()));
            factura.setIdMesa(new Mesa(dmnfactura.getIdMesa()));
            Cliente findCliente = clienteRepository.findByNombreAndApellido1(dmnfactura.getNombreCliente(), dmnfactura.getApellido1Cliente());
            if (findCliente == null) {
                findCliente = new Cliente();
                findCliente.setNombre(dmnfactura.getNombreCliente());
                findCliente.setApellido1(dmnfactura.getApellido1Cliente());
                findCliente.setApellido2(dmnfactura.getApellido2Cliente());
                findCliente = clienteRepository.saveAndFlush(findCliente);
            }
            factura.setIdCliente(findCliente);
            
            factura = facturaRepository.saveAndFlush(factura);
            
            DetalleFactura detalle;
            for (DmnDetalleFactura dmnDetalleFactura : dmnfactura.getDetalleFactura()) {
                detalle = new DetalleFactura();
                detalle.setIdFactura(factura);
                detalle.setIdCocinero(new Cocinero(dmnDetalleFactura.getIdCocinero()));
                detalle.setPlato(dmnDetalleFactura.getPlato());
                detalle.setImporte(dmnDetalleFactura.getValor());
                detalleFacturaRepository.saveAndFlush(detalle);
            }
        } catch (Exception e) {
            throw new SystemException("Error al registrar factura", e);
        }
    }

}
