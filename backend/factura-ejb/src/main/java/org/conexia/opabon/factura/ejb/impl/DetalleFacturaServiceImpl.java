/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.conexia.opabon.factura.ejb.impl;

import org.conexia.opabon.factura.ejb.DetalleFacturaService;
import org.conexia.opabon.factura.rpt.DetalleFacturaRepository;
import org.conexia.opabon.factura.rpt.domain.DetalleFactura;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author senneko
 */
@Service
public class DetalleFacturaServiceImpl implements DetalleFacturaService {
    
    @Autowired
    private DetalleFacturaRepository detalleFacturaRepository;

    @Override
    public <S extends DetalleFactura> S saveAndFlush(S s) {
        return detalleFacturaRepository.saveAndFlush(s);
    }
    
}
