/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.conexia.opabon.factura.ejb;

import java.util.List;
import org.conexia.opabon.factura.ejb.domain.DmnMesa;

/**
 *
 * @author senneko
 */
public interface MesaService {

    public List<DmnMesa> findAll();
    
}
