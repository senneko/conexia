/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.conexia.opabon.factura.ejb.impl;

import java.util.ArrayList;
import java.util.List;
import org.conexia.opabon.factura.ejb.MesaService;
import org.conexia.opabon.factura.ejb.domain.DmnMesa;
import org.conexia.opabon.factura.rpt.MesaRepository;
import org.conexia.opabon.factura.rpt.domain.Mesa;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author senneko
 */
@Service
public class MesaServiceImpl implements MesaService {

    @Autowired
    private MesaRepository mesaRepository;

    @Override
    public List<DmnMesa> findAll() {
        List<Mesa> rptMesa = mesaRepository.findAll();
        List<DmnMesa> dmnMesa = new ArrayList<>();
        for (Mesa mesa : rptMesa) {
            dmnMesa.add(DmnMesa.getDmnMesa(mesa));
        }
        return dmnMesa;
    }

}
