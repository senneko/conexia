/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.conexia.opabon.factura.ejb.domain;

import org.conexia.opabon.factura.rpt.domain.Mesa;

/**
 *
 * @author senneko
 */
public class DmnMesa {
    
    private Integer idMesa;
    private Integer numMaxPersonas;
    private String ubicacion;

    public Integer getIdMesa() {
        return idMesa;
    }

    public void setIdMesa(Integer idMesa) {
        this.idMesa = idMesa;
    }

    public Integer getNumMaxPersonas() {
        return numMaxPersonas;
    }

    public void setNumMaxPersonas(Integer numMaxPersonas) {
        this.numMaxPersonas = numMaxPersonas;
    }

    public String getUbicacion() {
        return ubicacion;
    }

    public void setUbicacion(String ubicacion) {
        this.ubicacion = ubicacion;
    }

    public static DmnMesa getDmnMesa(Mesa rptMesa) {
        DmnMesa mesa = new DmnMesa();
        mesa.setIdMesa(rptMesa.getIdMesa());
        mesa.setNumMaxPersonas(rptMesa.getNumMaxPersonas());
        mesa.setUbicacion(rptMesa.getUbicacion());
        return mesa;
    }
    
}
