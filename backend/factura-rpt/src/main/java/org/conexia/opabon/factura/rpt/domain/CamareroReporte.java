/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.conexia.opabon.factura.rpt.domain;

/**
 *
 * @author senneko
 */
public interface CamareroReporte {
    
    public String getNombre();
    
    public String getApellido();
    
    public String getMes();
    
    public String getTotal();
    
}
