/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.conexia.opabon.factura.rpt;

import org.conexia.opabon.factura.rpt.domain.Factura;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author senneko
 */
public interface FacturaRepository extends JpaRepository<Factura, Integer> {
}
