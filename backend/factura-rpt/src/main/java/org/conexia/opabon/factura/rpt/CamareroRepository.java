/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.conexia.opabon.factura.rpt;

import java.util.List;
import org.conexia.opabon.factura.rpt.domain.Camarero;
import org.conexia.opabon.factura.rpt.domain.CamareroReporte;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 *
 * @author senneko
 */
public interface CamareroRepository extends JpaRepository<Camarero, Integer> {

    @Query(nativeQuery = true, value = "SELECT "
            + "    cr.nombre as nombre, cr.apellido_1 as apellido, MONTHNAME(fr.fecha) as mes, SUM(df.importe) as total "
            + "FROM "
            + "    camarero cr "
            + "        LEFT JOIN "
            + "    factura fr ON cr.id_camarero = fr.id_camarero "
            + "        LEFT JOIN "
            + "    detalle_factura df ON fr.id_factura = df.id_factura "
            + "GROUP BY cr.nombre , cr.apellido_1 , MONTHNAME(fr.fecha) "
            + "ORDER BY 3 DESC")
    public List<CamareroReporte> getCamareroReporte();
}
