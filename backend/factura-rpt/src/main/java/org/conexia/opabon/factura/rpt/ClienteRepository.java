/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.conexia.opabon.factura.rpt;

import java.util.List;
import org.conexia.opabon.factura.rpt.domain.Cliente;
import org.conexia.opabon.factura.rpt.domain.ClienteReporte;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 *
 * @author senneko
 */
public interface ClienteRepository extends JpaRepository<Cliente, Integer> {

    @Query("select c from Cliente c where c.nombre = ?1 and c.apellido1 = ?2")
    public Cliente findByNombreAndApellido1(String nombre, String apellido1);

    @Query(nativeQuery = true, value = "SELECT "
            + "    cl.nombre as nombre, SUM(df.importe) as total "
            + "FROM "
            + "    cliente cl "
            + "        INNER JOIN "
            + "    factura fl ON fl.id_cliente = cl.id_cliente "
            + "        INNER JOIN "
            + "    detalle_factura df ON df.id_factura = fl.id_factura "
            + "GROUP BY cl.nombre "
            + "HAVING SUM(df.importe) > 100000")
    public List<ClienteReporte> getClienteReporte();

}
